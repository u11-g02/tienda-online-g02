import Usuario from "../models/Usuario.js";
import generarJWT from "../helpers/generaJWT.js";

const agregar = async (req, res) => {
  //evitar roles con el mismo nombre o nombre duplicado
  const { usuarioAcceso } = req.body;
  const existeUsuario = await Usuario.findOne({ usuarioAcceso });

  if (existeUsuario) {
      const error = new Error("Usuario ya esta registrado en la base de datos.")
      return res.status(400).json({ msg: error.message, ok: "No"});
  }

    try {
        const usuario = new Usuario(req.body);
        const usuarioAlmacenado = await usuario.save();
        res.json({ body: usuarioAlmacenado, ok: "SI", msg: "Registro creado correctamente." });
    } catch (error) {
        console.log(error);
    }
}

const listar = async (req, res) => {
    const usuarios = await Usuario.find().populate('idRol', {
        nombreRol: 1,
        _id: 0
    }).populate('idCiudad', {
        nombreCiudad: 1,
        _id: 0

    });
    res.json(usuarios);
}

const eliminar = async (req, res) => {
    console.log('metodo eliminar');
}

const editar = async (req, res) => {
    console.log('metodo editar');
}

const listarUno = async (req, res) => {
    console.log('metodo listarUno');
}

const autenticar = async (req, res) => {
    const { usuarioAcceso, claveAcceso} = req.body;

    //comprobar si el usuario existe
    const usuario = await Usuario.findOne({ usuarioAcceso });
    if (!usuario) {
        const error = new Error("El usuario no existe.");
        return res.status(404).json({msg: error.message, ok: "NO_EXISTE"});
    }

    //comprobar la contraseña
    if (await usuario.comprobarClave(claveAcceso)) {
        res.json({
            _id: usuario._id,
            nombresUsuario: usuario.nombresUsuario,
            usuarioAcceso: usuario.usuarioAcceso,
            tokenJwt: generarJWT(usuario._id)
        })
    } else {
        const error = new Error("La clave es incorrecta.");
        return res.status(400).json({msg: error.message, ok: "CLAVE_ERRONEA"});

    }
}

const crearCuenta = async (req, res) => {
    //evitar roles con el mismo nombre o nombre duplicado
    const { usuarioAcceso } = req.body;
    const existeUsuario = await Usuario.findOne({ usuarioAcceso });
  
    if (existeUsuario) {
        const error = new Error("Usuario ya esta registrado en la base de datos.")
        return res.status(400).json({ msg: error.message, ok: "No"});
    }
  
      try {
          const usuario = new Usuario(req.body);
          const usuarioAlmacenado = await usuario.save();
          res.json({ body: usuarioAlmacenado, ok: "SI" });
      } catch (error) {
          console.log(error);
      }
  }

export {
    agregar,
    listar,
    eliminar,
    editar,
    listarUno,
    autenticar,
    crearCuenta

}
import Ciudad from "../models/Ciudad.js";

const agregar = async (req, res) => {
    //evitar ciudades con el mismo nombre o nombre duplicado
    const { nombreCiudad } = req.body;
    const existeCiudad = await Ciudad.findOne({ nombreCiudad });

    if (existeCiudad) {
        const error = new Error("Ciudad ya esta registrado en la base de datos.")
        return res.status(400).json({ msg: error.message, ok: "No" });
    }
    try {
        const ciudad = new Ciudad(req.body);
        const ciudadAlmacenada = await ciudad.save();
        res.json({ body: ciudadAlmacenada, ok: "SI", msg: "Registro creado correctamente." })
    } catch (error) {
        console.log(error);
    }
}

const listar = async (req, res) => {
    const ciudades = await Ciudad.find();
    res.json(ciudades);

}

const eliminar = async (req, res) => {
       //recibir los parametros por la url
       const { id } = req.params;

       //validamos si existe el registro a eliminar
       const ciudad = await Ciudad.findById(id);
   
       if (!ciudad) {
           const error = new Error("Registro no encontrado.");
           return res.status(404).json({ msg: error.message, ok: "NO" });
       }
   
       try {
           await ciudad.deleteOne();
           res.json({ msg: "Registro eliminado correctamente.", ok: "SI" });
       } catch (error) {
           console.log(error);
       }
}

const editar = async (req, res) => {
     //recibir los parametros por la url
   const { id } = req.params;

   //validamos si existe el registro a eliminar
   const ciudad = await Ciudad.findById(id);

   if (!ciudad) {
       const error = new Error("Registro no encontrado.");
       return res.status(404).json({ msg: error.message, ok: "NO" });
   }

   //capturar los datos enviados desde el formulario
   ciudad.nombreCiudad = req.body.nombreCiudad || ciudad.nombreCiudad
   ciudad.estadoCiudad = req.body.estadoCiudad || ciudad.estadoCiudad

   try {
       const ciudadGuardada = await ciudad.save();
       res.json({ body: ciudadGuardada, ok: "SI", msg: "Registro editado correctamente." });
   } catch (error) {
       console.log(error);
   }
}

const listarUno = async (req, res) => {
   //recibir los parametros por la url
   const { id } = req.params;

   //validamos si existe el registro a eliminar
   const ciudad = await Ciudad.findById(id);

   if (!ciudad) {
       const error = new Error("Registro no encontrado.");
       return res.status(404).json({ msg: error.message, ok: "NO" });
   }

   res.json(ciudad);
}

export {
    agregar,
    listar,
    eliminar,
    editar,
    listarUno

}

import mongoose from "mongoose";

const pedidoSchema = mongoose.Schema({
    idCliente: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Usuario",
        require: true,
        trim: true
    },

    fechaPedido: {
        type: Date,
        require: true,
        trim: true
    },

    numeroPedido: {
        type: String,
        require: true,
        trim: true
    },

    estadoPedido: {
        type: Number,
        require: true,
        trim: true
    },

    totalPedido: {
        type: Number,
        require: true,
        trim: true
    },

    idTransaccion: {
        type: String,
        require: true,
        trim: true
    }
}, {
    timestamps: true
});

const Pedido = mongoose.model("Pedido", pedidoSchema);
export default Pedido;
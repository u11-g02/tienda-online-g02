import express from "express";
import dotenv from "dotenv"
import conectarDB from "./config/db.js";
import cors from 'cors';

//importamos los archivos de rutas
import rolesRoutes from "./routes/rolesRoutes.js";
import usuariosRoutes from './routes/usuariosRoutes.js';
import categoriasRoutes from './routes/categoriasRoutes.js';
import ciudadesRoutes from './routes/ciudadesRoutes.js';
import detallePedidoRoutes from './routes/detallePedidoRoutes.js';
import pedidosRoutes from './routes/pedidosRoutes.js';
import productosRoutes from './routes/productosRoutes.js';



//iniciamos el servidor de express
const app = express();
app.use(express.json());//para leer datos en formato json

//inicializa el uso de las variables de ambiente
dotenv.config();

//conectamos a la base de datos de mongodb
conectarDB();

//permitir conexiones externas con cors
const whiteList = [process.env.FRONTEND_URL]

const corsOptions = {
    origin: function (origin, callback) {
        if (whiteList.includes(origin)) {
            //puede consultar el api
            callback(null, true);
        } else {
            //no permitimos consultar el api
            callback(new Error("Error de Cors."));
        }
    },
};
app.use(cors(corsOptions));

//definicion de las rutas
app.use("/api/roles", rolesRoutes);
app.use("/api/usuarios", usuariosRoutes);
app.use("/api/categorias", categoriasRoutes);
app.use("/api/ciudades", ciudadesRoutes);
app.use("/api/detallepedido", detallePedidoRoutes);
app.use("/api/pedidos", pedidosRoutes);
app.use("/api/productos", productosRoutes);

// creamos una constante para controlar el Puerto 
const PORT = process.env.PORT || 4000;

app.listen(PORT, () =>{
    console.log(`servidor corriendo en el puerto ${PORT}`);

})
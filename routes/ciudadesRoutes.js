import express, { Router } from "express";
const router = express.Router();
import { agregar, listar, eliminar, editar, listarUno } from '../controllers/ciudadController.js';

router.post("/", agregar);
router.get("/", listar);
router.delete("/:id", eliminar);
router.put("/:id", editar);
router.get("/:id", listarUno);


export default router;
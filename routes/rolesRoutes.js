import express, { Router } from "express";
const router = express.Router();
import { agregar, listar, eliminar, editar, listarUno } from '../controllers/rolController.js';
import validarAutenticacion from "../middleware/validarAutenticacion.js"

//rutas privadas
router.post("/", validarAutenticacion, agregar);
router.get("/", validarAutenticacion, listar);
router.delete("/:id", validarAutenticacion, eliminar);
router.put("/:id", validarAutenticacion, editar);
router.get("/:id", validarAutenticacion, listarUno);

//rutas publicas
router.post("/login", agregar);
router.post("/crear-cuenta", agregar);

export default router;